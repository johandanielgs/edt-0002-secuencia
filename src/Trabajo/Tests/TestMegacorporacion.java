package Trabajo.Tests;

import Trabajo.Codes.Megacorporacion;
import Trabajo.Codes.Megacorporaciones;

/**
 * Test de prueba que simula operaciones entre conjuntos para la clase Megacorporacion
 * @author Adolfo Alejandro Arenas Ramos (1152370)
 * @author Johan Daniel García Salcedo (1152379)
 */
class TestMegacorporacion {

    /**
     * Método breve para comparar una compañía con otra e imprimir el proceso
     * @param a Objeto a
     * @param b Objeto b
     */
    public static void compareTwoMgCorporations(Object a, Object b){
        System.out.println("\n¿El objeto:\n\t"+a.toString()+"\nes igual a este objeto?:\n\t"+b.toString());
        if(a.equals(b)){
            System.out.println("\nSí.\n");
        }else{System.out.println("\nNo.\n");}

    }
    public static void main(String[] args) {
        Megacorporacion apple = new Megacorporacion("Apple", "Tim Cook", "Estados Unidos", "Cupertino");
        Megacorporacion microsoft = new Megacorporacion("Microsoft", "Satya Nadella", "Estados Unidos", "Silicon Valley");
        Megacorporacion ibm = new Megacorporacion("IBM", "Arvind Krishna", "Estados Unidos", "Nueva York");
        Megacorporacion tesla = new Megacorporacion("Tesla", "Elon Musk", "Estados Unidos", "Austin");
        Megacorporacion blackRock = new Megacorporacion("BlackRock", "Laurence D. Fink", "Estados unidos", "Nueva York");

        /* //Descomentar para probar la ejecución de la comparación 
        compareTwoMgCorporations(apple, tesla); */

        /* //Descomentar para imprimir los atributos de todos los objetos
        System.out.println(apple.toString()+microsoft.toString()+ibm.toString()+tesla.toString()+blackRock.toString()); */

        Megacorporaciones megacorporaciones1 = new Megacorporaciones(new Megacorporacion[]{apple, microsoft, ibm, tesla, blackRock});
        Megacorporaciones megacorporaciones2 = new Megacorporaciones(new Megacorporacion[]{apple, microsoft, ibm, tesla, blackRock});
        //System.out.println(megacorporaciones1.toString());
        megacorporaciones1.sort();
        //System.out.println(megacorporaciones1.toString());
        System.out.println(megacorporaciones1.getUnion(megacorporaciones2).toString());


    }
}