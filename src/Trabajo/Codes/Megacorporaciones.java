package Trabajo.Codes;

import Trabajo.Util.Secuencia;

public class Megacorporaciones {

    /* //Descomentar para declarar de forma completa
    private Secuencia<Megacorporacion> megacorporaciones = new Secuencia<Megacorporacion>(); */

    //Descomentar para declarar de forma simple
    private Secuencia<Megacorporacion> megacorporaciones;

    public Megacorporaciones(){
    }

    public Megacorporaciones(Megacorporacion[] corporaciones){
        this.megacorporaciones = new Secuencia<Megacorporacion>(corporaciones);
    }

    @Override
    public String toString() {
        String txt = "";
        for (int i = 0; i < this.megacorporaciones.size(); i++) {
            Megacorporacion x = this.megacorporaciones.get(i);
            txt += x.getNombre() + "\n";

        }
        return txt;
    }

    public void sort(){
        this.megacorporaciones.sort();
    }

    public Megacorporaciones getUnion(Megacorporaciones x){
        return null;
    }

    public Megacorporaciones getInterseccion(Megacorporaciones x){
        return null;
    }

    public Megacorporaciones getDiferencia(Megacorporaciones x){
        return null;
    }

    public Megacorporaciones getDiferenciaAsimetrica(Megacorporaciones x){
        return null;
    }

    public Megacorporaciones getConjuntosPares(Megacorporaciones x){
        return null;
    }
}
